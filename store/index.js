export const state = () => ({
  counter: 0,
  sortDir: true,
  menu: [],
  user: null,
  products: null,
  currentPage: 1,
  searchCats: null,
  currentCatID: null,
  searchVars: {},
  favourites: null,
  parentCat: null,
  cart: null,
  discount: null,
  children: null,
  maxPrice: null,
  minPrice: null,
  subcatToOpen: null,
  tempUser: null,
  backendUrl: null,
  filters: null,
  getProductsTrigger: false
})
export const mutations = {
  sortDirChange(state) {
    state.sortDir = !state.sortDir
  },
  triggerDownloadProducts(state){
    state.getProductsTrigger = !state.getProductsTrigger
  },
  increment(state) {
    state.counter++
  },
  setCurrentCat(state, catID) {
    state.currentCatID = catID
  },
  setMenu(state, menu) {
    state.menu = menu
  },
  resetFilterSliders(state) {
    for (const filterKey in state.filters) {
      if (state.filters[filterKey].type !== 'slider') continue
      state.filters[filterKey].current_min = state.filters[filterKey].min
      state.filters[filterKey].current_max = state.filters[filterKey].max
    }
  },
  normalizeMax(state, item) {
    state.filters[item].max = state.filters[item].current_max
  },
  setUser(state, user) {
    state.user = user
  },
  setProductQty(state, value) {
    const cartItem = state.cart.find((element) => element.id == value[0])
    if (value[1] > 0) {
      cartItem.qty = value[1]
    }
  },
  setTempUser(state, user) {
    this.tempUser = user
  },
  setBackendUrl(state, url) {
    this.backendUrl = url
  },
  setProducts(state, products) {
    state.products = products
  },
  setFilters(state, filters) {
    state.filters = filters
  },
  setSearchCats(state, categories) {
    state.searchCats = categories
  },
  setSearchVars(state, search) {
    state.searchVars = search
  },
  setSearchVar(state, val) {
    state.searchVars[val[0]] = val[1]
  },
  setFavourites(state, val) {
    state.favourites = val
  },
  setCart(state, val) {
    state.cart = val
  },
  setCatChildren(state, val) {
    state.children = val
  },
  setParentCategory(state, val) {
    state.parentCat = val
  },
  setMaxPrice(state, val) {
    state.maxPrice = val
  },
  setMinPrice(state, val) {
    state.minPrice = val
  },
  setSubcatToOpen(state, val) {
    state.subcatToOpen = val
  },
  setDiscount(state, val) {
    state.discount = val
  },
  setCurrentPage(state, page) {
    state.currentPage = page
  }
}
